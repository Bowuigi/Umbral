{ name = "umbral"
, dependencies =
  [ "arrays"
  , "console"
  , "control"
  , "deku"
  , "effect"
  , "either"
  , "foldable-traversable"
  , "foreign"
  , "integers"
  , "js-bigints"
  , "lists"
  , "maybe"
  , "node-buffer"
  , "node-fs"
  , "ordered-collections"
  , "parsing"
  , "prelude"
  , "rationals"
  , "simple-json"
  , "strings"
  , "transformers"
  , "tuples"
  ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
