module Format.TaxList where

import Prelude

import Common.Parse (parse)
import Common.Utils (To)
import Data.List (List)
import Data.List.NonEmpty (toList)
import Types.Formats (Tax)
import Format.TSV as TSV
import Parsing.Combinators (many1Till)

tax :: To Tax
tax = do
  _name <- TSV.str
  TSV.sep
  _type <- parse
  TSV.sep
  _amount <- parse
  TSV.sep
  _condition <- parse
  TSV.sep
  _overrides <- TSV.str
  pure {
    name: _name,
    type: _type,
    amount: _amount,
    condition: _condition,
    overrides: _overrides
  }
  <* TSV.nl

taxList :: forall a. To a -> To (List Tax)
taxList end = TSV.header 5 *> (toList <$> many1Till tax end)

