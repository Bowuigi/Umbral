module Format.Info where

import Prelude

import Data.List (intercalate)
import Data.List.Lazy (fold, replicate)
import Data.List.Types (toList)
import Foreign (ForeignError(..), MultipleErrors)

-- JSON error showing
showErrors :: MultipleErrors -> String
showErrors e = intercalate "\n\n" $ map (showErrors' 1) (toList e)
  where
  showErrors' :: Int -> ForeignError -> String
  showErrors' indent (ErrorAtProperty s rest) =
    "On property \""
      <> s
      <> "\"\n"
      <> fold (replicate indent "  ")
      <> showErrors' (indent + 1) rest
  showErrors' indent (ErrorAtIndex n rest) =
    "On index "
      <> show n
      <> "\n"
      <> fold (replicate indent "  ")
      <> showErrors' (indent + 1) rest
  showErrors' indent (TypeMismatch t t') =
    fold (replicate indent "  ")
      <> "Unexpected type "
      <> t'
      <> ", expecting "
      <> t
  showErrors' indent (ForeignError s) =
    fold (replicate indent "  ")
      <> s

