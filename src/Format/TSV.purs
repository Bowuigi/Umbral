module Format.TSV where

import Prelude

import Common.Utils (To, fromChars)
import Data.List (List, length)
import Data.List.Types (NonEmptyList)
import Parsing (fail)
import Parsing.Combinators (many, many1, sepBy, sepBy1)
import Parsing.String (char)
import Parsing.String.Basic (noneOf)

type Values = List String
type TSV = NonEmptyList Values

sep :: To Unit
sep = void $ char '\t'

nl :: To Unit
nl = void $ char '\n'

str :: To String
str = fromChars <$> many1 (noneOf ['\t', '\n']) 

line :: Int -> To Values
line n = do
  values <- (fromChars <$> many (noneOf ['\t','\n'])) `sepBy` sep
  let got = length values
  if got /= n
    then fail $ "Expected " <> show n <> " values, got " <> show got
    else pure values

header :: Int -> To Unit
header n = void $ line n *> char '\n'

tsv :: Int -> To TSV
tsv n = line n `sepBy1` char '\n'

tsvWithHeader :: Int -> To TSV
tsvWithHeader n = header n *> tsv n

