module Format.CostList where

import Prelude

import Common.Parse (parse)
import Common.Utils (To)
import Data.List (List)
import Data.List.NonEmpty (toList)
import Format.TSV as TSV
import Parsing.Combinators (many1Till)
import Types.Formats (Cost)

cost :: To Cost
cost = do
  _source <- TSV.str
  TSV.sep
  _id <- parse
  TSV.sep
  _category <- TSV.str
  TSV.sep
  _amount <- parse
  TSV.sep
  _unit <- parse
  pure {
    source: _source,
    id: _id,
    category: _category,
    amount: _amount,
    unit: _unit
  }
  <* TSV.nl

costList :: forall a. To a -> To (List Cost)
costList end = TSV.header 5 *> (toList <$> many1Till cost end)

