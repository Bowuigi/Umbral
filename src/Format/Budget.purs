module Format.Budget where

import Prelude

import Common.Utils (To, fromChars)
import Control.Alt ((<|>))
import Data.Either (either)
import Data.List (List)
import Data.List.NonEmpty (toList)
import Format.CostList (costList)
import Format.Info (showErrors)
import Format.PriceList (priceList)
import Format.TaxList (taxList)
import Parsing (fail)
import Parsing.Combinators (lookAhead, many1, many1Till)
import Parsing.String (anyChar, char, eof, satisfy, string)
import Parsing.String.Basic (oneOf)
import Simple.JSON (readJSON)
import Types.Formats (Prices, Budget, Tax, Info, Cost)

{-
  Info, taxes, costs:
  ## %s
  Price Lists:
  #! %s
-}

header :: String -> To Unit
header s = do
  void $ string $ "## " <> s <> "\n"

nextHeader :: To Unit
nextHeader = void $ lookAhead (string "#" *> (oneOf ['!', '#']))

info :: To Unit -> To Info
info end = do
  header "Info"
  json <- fromChars <$> many1Till anyChar end
  either (fail <<< showErrors) pure $ readJSON json

taxes :: To Unit -> To (List Tax)
taxes end = do
  header "Taxes"
  taxList end

costs :: To Unit -> To (List Cost)
costs end = do
  header "Costs"
  costList end

prices :: forall a. To a -> To Prices
prices end = do
  hdr <- priceHeader
  void $ char '\n'
  prs <- priceList end
  pure {
    name: hdr,
    prices: prs
  }
  where
    priceHeader :: To String
    priceHeader = do
       void $ string "#! "
       fromChars <<< toList <$> many1 (satisfy (_ /= '\n'))

budget :: To Budget
budget = do
  i <- info nextHeader
  t <- taxes nextHeader
  c <- costs nextHeader
  ps <- many1 $ prices (nextHeader <|> eof)
  pure {
    info: i,
    taxes: t,
    costs: c,
    priceLists: toList ps
  }

