module Format.PriceList where

import Prelude

import Common.Utils (To)
import Types.Formats (Price)
import Data.List (List)
import Data.List.NonEmpty (toList)
import Common.Parse (parse)
import Parsing.Combinators (many1Till)
import Format.TSV as TSV

price :: To Price
price = do
  _id <- parse
  TSV.sep
  _name <- TSV.str
  TSV.sep
  _upp <- parse
  TSV.sep
  _unit <- parse
  TSV.sep
  _ppp <- parse
  TSV.sep
  _currency <- parse
  pure {
    id: _id,
    name: _name,
    upp: _upp,
    unit: _unit,
    ppp: _ppp,
    currency: _currency
  }
  <* TSV.nl

priceList :: forall a. To a -> To (List Price)
priceList end = TSV.header 6 *> (toList <$> many1Till price end)

