-- | Export a Budget to various formats
module Format.Export (export, total, exportTSV) where

import Prelude hiding (unit, Unit)

import Common.Units (divRemQ)
import Common.Utils (lookupBy, (^|!))
import Control.Monad.Except (Except)
import Data.Foldable (intercalate)
import Data.List (List)
import Data.Map (fromFoldableWith)
import Data.Traversable (sequence)
import Data.Tuple (Tuple(..))
import Types.Formats (Budget, Exported, Total)

export :: Budget -> Except String (List Exported)
export {costs, priceLists} = sequence $ process <$> costs
  where
    process {amount, category, id, source, unit} =
      do
        -- Look up the needed price list in the source list
        src <- lookupBy (_.name >>> (_ == source)) priceLists
              ^|! ("No source named " <> source)
        -- Look up the needed item in the source list
        item <- lookupBy (_.id >>> (_ == id)) src.prices
               ^|! "No item id " <> show id <> " in source " <> source
        (Tuple bought unused) <- Tuple amount unit `divRemQ` Tuple item.upp item.unit
                                ^|! "Invalid unit conversion: " <> show item.unit <> " -> " <> show unit
        let price = item.ppp * bought
        pure {
          category,
          unit,
          amount,
          name: item.name,
          pkg_unit: item.unit,
          upp: item.upp,
          ppp: item.ppp,
          currency: item.currency,
          bought,
          unused,
          price
        }

total :: List Exported -> Total
total = fromFoldableWith (+) <<< map (\{price, currency} -> Tuple currency price)

tsvHeader :: Array String
tsvHeader = [
  "Category",
  "Name",
  "Amount",
  "Unit",
  "Units Per Package",
  "Package Unit",
  "Price Per Package",
  "Currency",
  "Price",
  "Bought packages",
  "Unused units"
]

tsvLine :: Exported -> Array String
tsvLine {category, name,
         amount,   unit,
         pkg_unit, upp,
         ppp,      currency,
         price,    bought,
         unused} = [
                     category,
                     name,
                     show amount,
                     show unit,
                     show upp,
                     show pkg_unit,
                     show ppp,
                     show currency,
                     show price,
                     show bought,
                     show unused
                   ]

exportTSV :: List Exported -> String
exportTSV e = intercalate "\t" tsvHeader <> "\n" <>
              (intercalate "\n" $ intercalate "\t" <$> tsvLine <$> e)

