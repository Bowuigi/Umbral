module Main where

{-
import Prelude hiding (unit)
import Prelude (unit) as Prelude

import Types.Formats (Exported)
import Data.List (List, (:))
import Control.Plus (empty)
import QualifiedDo.Alt as Alt
import Data.Array (replicate, singleton, fromFoldable)
import Deku.Attribute ((!:=))
import Deku.Attributes (klass)
import Deku.Listeners (click)
import Deku.Control (text_, (<#~>))
import Deku.Core (Nut, fixed)
import Deku.DOM as D
import Deku.Toplevel (runInBody)
import Effect (Effect)
import Deku.Do as Deku
import Data.Tuple.Nested ((/\))
import Deku.Hooks (useState)

data Section = Information
             | Costs
             | PriceLists
             | Taxes
             | Results

derive instance sectionEq :: Eq Section

main :: Effect Unit
main = runInBody container

container :: Nut
container = Deku.do
  setSection /\ getSection <- useState Results
  fixed [
    sidebar setSection getSection,
    D.div (D.Class !:= "ml-[calc(2.8in+1em)] mr-[2em] mt-[1em] text-white text-justify") [
      getSection <#~>
        case _ of
          Information -> text_ "Information"
          Costs -> text_ "Costs"
          PriceLists -> text_ "PriceLists"
          Taxes -> text_ "Taxes"
          Results -> results
    ]
  ]

results :: Nut
results = fixed [
            D.h1 (D.Class !:= "text-2xl underline font-bold mb-[1em]") [text_ "Budget results"],
            table
          ]

largeText :: String
largeText = "text-3xl"

sidebar :: _ -> _ -> Nut
sidebar setSection getSection =
  D.div (
    D.Class !:= "top-0 left-0 fixed w-[2.7in] h-screen p-[0.5em] h-full bg-slate-900"
  ) [
    D.br_ [],
    D.center_ [
      D.span (D.Class !:= largeText) [text_ "[U]"],
      text_ " ",
      D.span (D.Class !:= largeText) [text_ "Umbral"]
    ],
    D.br_ [],
    sectionButton "Information" Information,
    sectionButton "Costs" Costs,
    sectionButton "Price lists" PriceLists,
    sectionButton "Taxes" Taxes,
    sectionButton "Results" Results,
    D.div (D.Class !:= "absolute m-[10pt] bottom-[25pt]") [
      D.button (
        D.Class !:= "mb-[0.7em] " <> buttonStyle
      ) [text_ "Import"],
      D.button (
        D.Class !:= buttonStyle
      ) [text_ "Export"]
    ]
  ]
  where
    sectionButton :: String -> Section -> Nut
    sectionButton name sec =
      let baseStyle = "block w-full text-left mb-[0.5em] p-[0.5em]" in
        D.button Alt.do
                     klass $ getSection <#> (_ == sec) <#>
                               if _ then "border-r-[0.4em] border-blue-500 bg-gradient-to-r from-transparent to-blue-900 " <> baseStyle
                                    else "hover:bg-gray-700 " <> baseStyle
                     D.OnClick !:= (pure sec >>= setSection)
          [text_ name]
    buttonStyle :: String
    buttonStyle = "w-[9em] bg-blue-700 hover:bg-blue-500 p-[0.5em] rounded-3xl"

table :: Nut
table = D.table (D.Class !:= "whitespace-nowrap") [
    D.tr (D.Class !:= "gap-[1em]") (replicate 8 (D.th (D.Class !:= "px-[0.5em] underline text-center") [text_ "Header"])),
    D.tr empty (replicate 8 (D.td (D.Class !:= "p-[0.5em]") [text_ "Content 1"])),
    D.tr empty (replicate 8 (D.td (D.Class !:= "p-[0.5em]") [text_ "Content 2"])),
    D.tr empty (replicate 8 (D.td (D.Class !:= "p-[0.5em]") [text_ "Content 3"])),
    D.tr empty (replicate 8 (D.td (D.Class !:= "p-[0.5em]") [text_ "Content 4"]))
  ]

-- Exporting to HTML tables

entry :: Exported -> Nut
entry {category, name,
       amount,   unit,
       price, currency,
       bought, unused} = D.tr empty [
         td category,
         td name,
         td $ show amount,
         td $ show unit,
         td $ show price,
         td $ show currency,
         td $ show bought,
         td $ show unused
       ]
  where
    td = D.td (D.Class !:= "p-[0.5em]") <<< singleton <<< text_

exportHTML :: List Exported -> Nut
exportHTML l = D.table (D.Class !:= "whitespace-nowrap") $ fromFoldable $ header : (entry <$> l)
  where
    header = D.tr (D.Class !:= "gap-[1em]") $ (D.th (D.Class !:= "px-[0.5em] underline text-center") <<< singleton <<< text_) <$> ["Category", "Name", "Amount", "Unit", "Price", "Currency", "Bought packages", "Unused units"]
