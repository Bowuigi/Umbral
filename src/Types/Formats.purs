module Types.Formats where

import Prelude

import Common.BigRational (BigRational)
import Common.Parse (class Parse, genericParse)
import Common.Units (DerivedUnit)
import Data.Generic.Rep (class Generic)
import Data.List (List)
import Data.Map (Map)
import Data.Maybe (Maybe)
import Data.Show.Generic (genericShow)
import Foreign (readString)
import Simple.JSON as JSON
import Types.CurrencyCodes (Currency)

data TaxType = Fixed Currency | Percentage

derive instance taxTypeGeneric :: Generic TaxType _
instance taxTypeShow :: Show TaxType where show = genericShow
instance taxTypeParse :: Parse TaxType where parse = genericParse

data TaxCondition
  = None
  | Method PaymentMethod
  | Source String
  | Currency (List Currency)
  | NotCurrency (List Currency)
  | Exceeds BigRational Currency
  | Below BigRational Currency
-- Parse does not allow recursion in this way, not sure why
--  | All (List TaxCondition)
--  | Any (List TaxCondition)

derive instance taxConditionGeneric :: Generic TaxCondition _
instance taxConditionShow :: Show TaxCondition where show = genericShow
instance taxConditionParse :: Parse TaxCondition where parse = genericParse

type Tax = {
  name :: String,
  type :: TaxType,
  amount :: BigRational,
  condition :: TaxCondition,
  overrides :: String
}

data PaymentMethod = Credit | Debit | Cash | Other String

derive instance paymentMethodGeneric :: Generic PaymentMethod _
instance paymentMethodShow :: Show PaymentMethod where show = genericShow
instance pmParse :: Parse PaymentMethod where parse = genericParse

instance paymentMethodRF :: JSON.ReadForeign PaymentMethod where
  readImpl f = toPM <$> readString f
    where
    toPM "credit" = Credit
    toPM "debit" = Debit
    toPM "cash" = Cash
    toPM s = Other s

type Source = {
  url :: String,
  name :: String,
  paymentMethod :: PaymentMethod
}

type Info = {
  name :: String,
  supervisor :: Maybe String,
  location :: Maybe String,
  company :: Maybe String,
  version :: Int,
  sources :: Array Source,
  currency :: Currency
}

type Cost = {
  source :: String,
  id :: Int,
  category :: String,
  amount :: BigRational,
  unit :: DerivedUnit
}

type Price = {
  id :: Int,
  name :: String,
  upp :: BigRational, -- Units Per Package
  unit :: DerivedUnit,
  ppp :: BigRational, -- Price Per Package
  currency :: Currency
}

type Prices = {
  name :: String,
  prices :: List Price
}

type Budget = {
  info :: Info,
  taxes :: List Tax,
  costs :: List Cost,
  priceLists :: List Prices
}

-- Note: The examples are all connected together
type Exported = {
  category :: String,
  name :: String,
  -- How many units are needed, Ex: 7un
  amount :: BigRational,
  unit :: DerivedUnit,
  -- Package price, Ex: 6un x $70
  upp :: BigRational,
  pkg_unit :: DerivedUnit,
  ppp :: BigRational,
  currency :: Currency,
  -- Full price, Ex: 6un x $70, we need 7 so we buy 2 packages.
  price :: BigRational,
  -- Bought packages, Ex: 2
  bought :: BigRational,
  -- How many common/SI units are left unused, Ex: 5 out of 6un
  unused :: BigRational
}

type Total = Map Currency BigRational

