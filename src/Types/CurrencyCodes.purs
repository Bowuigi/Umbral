{-
  Extract of currency-codes-3.0.0.1 containing only the currency codes type,
  renamed from Alpha to Currency.
  It also contains Purescript instances for various typeclasses.

  Original license:

  MIT License
  
  Copyright (c) 2017-2018 Chordify
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
-}

module Types.CurrencyCodes (Currency) where

import Prelude

import Common.Utils (fromChars)
import Common.Parse (class Parse)
import Data.Maybe (Maybe(..), maybe)
import Foreign (readString, ForeignError(..), fail)
import Parsing (fail) as P
import Parsing.String.Basic (upper)
import Simple.JSON (class ReadForeign)

-- | A type which represents ISO 4217 alphabetic codes as an enum
-- | Renamed from Alpha to Currency
data Currency
  = AED -- ^ UAE Dirham
  | AFN -- ^ Afghani
  | ALL -- ^ Lek
  | AMD -- ^ Armenian Dram
  | ANG -- ^ Netherlands Antillean Guilder
  | AOA -- ^ Kwanza
  | ARS -- ^ Argentine Peso
  | AUD -- ^ Australian Dollar
  | AWG -- ^ Aruban Florin
  | AZN -- ^ Azerbaijan Manat
  | BAM -- ^ Convertible Mark
  | BBD -- ^ Barbados Dollar
  | BDT -- ^ Taka
  | BGN -- ^ Bulgarian Lev
  | BHD -- ^ Bahraini Dinar
  | BIF -- ^ Burundi Franc
  | BMD -- ^ Bermudian Dollar
  | BND -- ^ Brunei Dollar
  | BOB -- ^ Boliviano
  | BOV -- ^ Mvdol
  | BRL -- ^ Brazilian Real
  | BSD -- ^ Bahamian Dollar
  | BTN -- ^ Ngultrum
  | BWP -- ^ Pula
  | BYN -- ^ Belarusian Ruble
  | BZD -- ^ Belize Dollar
  | CAD -- ^ Canadian Dollar
  | CDF -- ^ Congolese Franc
  | CHE -- ^ WIR Euro
  | CHF -- ^ Swiss Franc
  | CHW -- ^ WIR Franc
  | CLF -- ^ Unidad de Fomento
  | CLP -- ^ Chilean Peso
  | CNY -- ^ Yuan Renminbi
  | COP -- ^ Colombian Peso
  | COU -- ^ Unidad de Valor Real
  | CRC -- ^ Costa Rican Colon
  | CUC -- ^ Peso Convertible
  | CUP -- ^ Cuban Peso
  | CVE -- ^ Cabo Verde Escudo
  | CZK -- ^ Czech Koruna
  | DJF -- ^ Djibouti Franc
  | DKK -- ^ Danish Krone
  | DOP -- ^ Dominican Peso
  | DZD -- ^ Algerian Dinar
  | EGP -- ^ Egyptian Pound
  | ERN -- ^ Nakfa
  | ETB -- ^ Ethiopian Birr
  | EUR -- ^ Euro
  | FJD -- ^ Fiji Dollar
  | FKP -- ^ Falkland Islands Pound
  | GBP -- ^ Pound Sterling
  | GEL -- ^ Lari
  | GHS -- ^ Ghana Cedi
  | GIP -- ^ Gibraltar Pound
  | GMD -- ^ Dalasi
  | GNF -- ^ Guinean Franc
  | GTQ -- ^ Quetzal
  | GYD -- ^ Guyana Dollar
  | HKD -- ^ Hong Kong Dollar
  | HNL -- ^ Lempira
  | HRK -- ^ Kuna
  | HTG -- ^ Gourde
  | HUF -- ^ Forint
  | IDR -- ^ Rupiah
  | ILS -- ^ New Israeli Sheqel
  | INR -- ^ Indian Rupee
  | IQD -- ^ Iraqi Dinar
  | IRR -- ^ Iranian Rial
  | ISK -- ^ Iceland Krona
  | JMD -- ^ Jamaican Dollar
  | JOD -- ^ Jordanian Dinar
  | JPY -- ^ Yen
  | KES -- ^ Kenyan Shilling
  | KGS -- ^ Som
  | KHR -- ^ Riel
  | KMF -- ^ Comorian Franc
  | KPW -- ^ North Korean Won
  | KRW -- ^ Won
  | KWD -- ^ Kuwaiti Dinar
  | KYD -- ^ Cayman Islands Dollar
  | KZT -- ^ Tenge
  | LAK -- ^ Lao Kip
  | LBP -- ^ Lebanese Pound
  | LKR -- ^ Sri Lanka Rupee
  | LRD -- ^ Liberian Dollar
  | LSL -- ^ Loti
  | LYD -- ^ Libyan Dinar
  | MAD -- ^ Moroccan Dirham
  | MDL -- ^ Moldovan Leu
  | MGA -- ^ Malagasy Ariary
  | MKD -- ^ Denar
  | MMK -- ^ Kyat
  | MNT -- ^ Tugrik
  | MOP -- ^ Pataca
  | MRO -- ^ Ouguiya
  | MUR -- ^ Mauritius Rupee
  | MVR -- ^ Rufiyaa
  | MWK -- ^ Malawi Kwacha
  | MXN -- ^ Mexican Peso
  | MXV -- ^ Mexican Unidad de Inversion (UDI)
  | MYR -- ^ Malaysian Ringgit
  | MZN -- ^ Mozambique Metical
  | NAD -- ^ Namibia Dollar
  | NGN -- ^ Naira
  | NIO -- ^ Cordoba Oro
  | NOK -- ^ Norwegian Krone
  | NPR -- ^ Nepalese Rupee
  | NZD -- ^ New Zealand Dollar
  | OMR -- ^ Rial Omani
  | PAB -- ^ Balboa
  | PEN -- ^ Sol
  | PGK -- ^ Kina
  | PHP -- ^ Philippine Peso
  | PKR -- ^ Pakistan Rupee
  | PLN -- ^ Zloty
  | PYG -- ^ Guarani
  | QAR -- ^ Qatari Rial
  | RON -- ^ Romanian Leu
  | RSD -- ^ Serbian Dinar
  | RUB -- ^ Russian Ruble
  | RWF -- ^ Rwanda Franc
  | SAR -- ^ Saudi Riyal
  | SBD -- ^ Solomon Islands Dollar
  | SCR -- ^ Seychelles Rupee
  | SDG -- ^ Sudanese Pound
  | SEK -- ^ Swedish Krona
  | SGD -- ^ Singapore Dollar
  | SHP -- ^ Saint Helena Pound
  | SLL -- ^ Leone
  | SOS -- ^ Somali Shilling
  | SRD -- ^ Surinam Dollar
  | SSP -- ^ South Sudanese Pound
  | STD -- ^ Dobra
  | SVC -- ^ El Salvador Colon
  | SYP -- ^ Syrian Pound
  | SZL -- ^ Lilangeni
  | THB -- ^ Baht
  | TJS -- ^ Somoni
  | TMT -- ^ Turkmenistan New Manat
  | TND -- ^ Tunisian Dinar
  | TOP -- ^ Pa’anga
  | TRY -- ^ Turkish Lira
  | TTD -- ^ Trinidad and Tobago Dollar
  | TWD -- ^ New Taiwan Dollar
  | TZS -- ^ Tanzanian Shilling
  | UAH -- ^ Hryvnia
  | UGX -- ^ Uganda Shilling
  | USD -- ^ US Dollar
  | USN -- ^ US Dollar (Next day)
  | UYI -- ^ Uruguay Peso en Unidades Indexadas (URUIURUI)
  | UYU -- ^ Peso Uruguayo
  | UZS -- ^ Uzbekistan Sum
  | VEF -- ^ Bolívar
  | VND -- ^ Dong
  | VUV -- ^ Vatu
  | WST -- ^ Tala
  | XAF -- ^ CFA Franc BEAC
  | XAG -- ^ Silver
  | XAU -- ^ Gold
  | XBA -- ^ Bond Markets Unit European Composite Unit (EURCO)
  | XBB -- ^ Bond Markets Unit European Monetary Unit (E.M.U.-6)
  | XBC -- ^ Bond Markets Unit European Unit of Account 9 (E.U.A.-9)
  | XBD -- ^ Bond Markets Unit European Unit of Account 17 (E.U.A.-17)
  | XCD -- ^ East Caribbean Dollar
  | XDR -- ^ SDR (Special Drawing Right)
  | XOF -- ^ CFA Franc BCEAO
  | XPD -- ^ Palladium
  | XPF -- ^ CFP Franc
  | XPT -- ^ Platinum
  | XSU -- ^ Sucre
  | XTS -- ^ Codes specifically reserved for testing purposes
  | XUA -- ^ ADB Unit of Account
  | XXX -- ^ The codes assigned for transactions where no currency is involved
  | YER -- ^ Yemeni Rial
  | ZAR -- ^ Rand
  | ZMW -- ^ Zambian Kwacha
  | ZWL -- ^ Zimbabwe Dollar

derive instance currencyEq :: Eq Currency
derive instance ordCurrency :: Ord Currency

-- Generic instances take forever to compile for large sum types, so we do them by hand.

instance currencyShow :: Show Currency where
  show AED = "AED"
  show AFN = "AFN"
  show ALL = "ALL"
  show AMD = "AMD"
  show ANG = "ANG"
  show AOA = "AOA"
  show ARS = "ARS"
  show AUD = "AUD"
  show AWG = "AWG"
  show AZN = "AZN"
  show BAM = "BAM"
  show BBD = "BBD"
  show BDT = "BDT"
  show BGN = "BGN"
  show BHD = "BHD"
  show BIF = "BIF"
  show BMD = "BMD"
  show BND = "BND"
  show BOB = "BOB"
  show BOV = "BOV"
  show BRL = "BRL"
  show BSD = "BSD"
  show BTN = "BTN"
  show BWP = "BWP"
  show BYN = "BYN"
  show BZD = "BZD"
  show CAD = "CAD"
  show CDF = "CDF"
  show CHE = "CHE"
  show CHF = "CHF"
  show CHW = "CHW"
  show CLF = "CLF"
  show CLP = "CLP"
  show CNY = "CNY"
  show COP = "COP"
  show COU = "COU"
  show CRC = "CRC"
  show CUC = "CUC"
  show CUP = "CUP"
  show CVE = "CVE"
  show CZK = "CZK"
  show DJF = "DJF"
  show DKK = "DKK"
  show DOP = "DOP"
  show DZD = "DZD"
  show EGP = "EGP"
  show ERN = "ERN"
  show ETB = "ETB"
  show EUR = "EUR"
  show FJD = "FJD"
  show FKP = "FKP"
  show GBP = "GBP"
  show GEL = "GEL"
  show GHS = "GHS"
  show GIP = "GIP"
  show GMD = "GMD"
  show GNF = "GNF"
  show GTQ = "GTQ"
  show GYD = "GYD"
  show HKD = "HKD"
  show HNL = "HNL"
  show HRK = "HRK"
  show HTG = "HTG"
  show HUF = "HUF"
  show IDR = "IDR"
  show ILS = "ILS"
  show INR = "INR"
  show IQD = "IQD"
  show IRR = "IRR"
  show ISK = "ISK"
  show JMD = "JMD"
  show JOD = "JOD"
  show JPY = "JPY"
  show KES = "KES"
  show KGS = "KGS"
  show KHR = "KHR"
  show KMF = "KMF"
  show KPW = "KPW"
  show KRW = "KRW"
  show KWD = "KWD"
  show KYD = "KYD"
  show KZT = "KZT"
  show LAK = "LAK"
  show LBP = "LBP"
  show LKR = "LKR"
  show LRD = "LRD"
  show LSL = "LSL"
  show LYD = "LYD"
  show MAD = "MAD"
  show MDL = "MDL"
  show MGA = "MGA"
  show MKD = "MKD"
  show MMK = "MMK"
  show MNT = "MNT"
  show MOP = "MOP"
  show MRO = "MRO"
  show MUR = "MUR"
  show MVR = "MVR"
  show MWK = "MWK"
  show MXN = "MXN"
  show MXV = "MXV"
  show MYR = "MYR"
  show MZN = "MZN"
  show NAD = "NAD"
  show NGN = "NGN"
  show NIO = "NIO"
  show NOK = "NOK"
  show NPR = "NPR"
  show NZD = "NZD"
  show OMR = "OMR"
  show PAB = "PAB"
  show PEN = "PEN"
  show PGK = "PGK"
  show PHP = "PHP"
  show PKR = "PKR"
  show PLN = "PLN"
  show PYG = "PYG"
  show QAR = "QAR"
  show RON = "RON"
  show RSD = "RSD"
  show RUB = "RUB"
  show RWF = "RWF"
  show SAR = "SAR"
  show SBD = "SBD"
  show SCR = "SCR"
  show SDG = "SDG"
  show SEK = "SEK"
  show SGD = "SGD"
  show SHP = "SHP"
  show SLL = "SLL"
  show SOS = "SOS"
  show SRD = "SRD"
  show SSP = "SSP"
  show STD = "STD"
  show SVC = "SVC"
  show SYP = "SYP"
  show SZL = "SZL"
  show THB = "THB"
  show TJS = "TJS"
  show TMT = "TMT"
  show TND = "TND"
  show TOP = "TOP"
  show TRY = "TRY"
  show TTD = "TTD"
  show TWD = "TWD"
  show TZS = "TZS"
  show UAH = "UAH"
  show UGX = "UGX"
  show USD = "USD"
  show USN = "USN"
  show UYI = "UYI"
  show UYU = "UYU"
  show UZS = "UZS"
  show VEF = "VEF"
  show VND = "VND"
  show VUV = "VUV"
  show WST = "WST"
  show XAF = "XAF"
  show XAG = "XAG"
  show XAU = "XAU"
  show XBA = "XBA"
  show XBB = "XBB"
  show XBC = "XBC"
  show XBD = "XBD"
  show XCD = "XCD"
  show XDR = "XDR"
  show XOF = "XOF"
  show XPD = "XPD"
  show XPF = "XPF"
  show XPT = "XPT"
  show XSU = "XSU"
  show XTS = "XTS"
  show XUA = "XUA"
  show XXX = "XXX"
  show YER = "YER"
  show ZAR = "ZAR"
  show ZMW = "ZMW"
  show ZWL = "ZWL"

currency :: String -> Maybe Currency
currency "AED" = Just AED
currency "AFN" = Just AFN
currency "ALL" = Just ALL
currency "AMD" = Just AMD
currency "ANG" = Just ANG
currency "AOA" = Just AOA
currency "ARS" = Just ARS
currency "AUD" = Just AUD
currency "AWG" = Just AWG
currency "AZN" = Just AZN
currency "BAM" = Just BAM
currency "BBD" = Just BBD
currency "BDT" = Just BDT
currency "BGN" = Just BGN
currency "BHD" = Just BHD
currency "BIF" = Just BIF
currency "BMD" = Just BMD
currency "BND" = Just BND
currency "BOB" = Just BOB
currency "BOV" = Just BOV
currency "BRL" = Just BRL
currency "BSD" = Just BSD
currency "BTN" = Just BTN
currency "BWP" = Just BWP
currency "BYN" = Just BYN
currency "BZD" = Just BZD
currency "CAD" = Just CAD
currency "CDF" = Just CDF
currency "CHE" = Just CHE
currency "CHF" = Just CHF
currency "CHW" = Just CHW
currency "CLF" = Just CLF
currency "CLP" = Just CLP
currency "CNY" = Just CNY
currency "COP" = Just COP
currency "COU" = Just COU
currency "CRC" = Just CRC
currency "CUC" = Just CUC
currency "CUP" = Just CUP
currency "CVE" = Just CVE
currency "CZK" = Just CZK
currency "DJF" = Just DJF
currency "DKK" = Just DKK
currency "DOP" = Just DOP
currency "DZD" = Just DZD
currency "EGP" = Just EGP
currency "ERN" = Just ERN
currency "ETB" = Just ETB
currency "EUR" = Just EUR
currency "FJD" = Just FJD
currency "FKP" = Just FKP
currency "GBP" = Just GBP
currency "GEL" = Just GEL
currency "GHS" = Just GHS
currency "GIP" = Just GIP
currency "GMD" = Just GMD
currency "GNF" = Just GNF
currency "GTQ" = Just GTQ
currency "GYD" = Just GYD
currency "HKD" = Just HKD
currency "HNL" = Just HNL
currency "HRK" = Just HRK
currency "HTG" = Just HTG
currency "HUF" = Just HUF
currency "IDR" = Just IDR
currency "ILS" = Just ILS
currency "INR" = Just INR
currency "IQD" = Just IQD
currency "IRR" = Just IRR
currency "ISK" = Just ISK
currency "JMD" = Just JMD
currency "JOD" = Just JOD
currency "JPY" = Just JPY
currency "KES" = Just KES
currency "KGS" = Just KGS
currency "KHR" = Just KHR
currency "KMF" = Just KMF
currency "KPW" = Just KPW
currency "KRW" = Just KRW
currency "KWD" = Just KWD
currency "KYD" = Just KYD
currency "KZT" = Just KZT
currency "LAK" = Just LAK
currency "LBP" = Just LBP
currency "LKR" = Just LKR
currency "LRD" = Just LRD
currency "LSL" = Just LSL
currency "LYD" = Just LYD
currency "MAD" = Just MAD
currency "MDL" = Just MDL
currency "MGA" = Just MGA
currency "MKD" = Just MKD
currency "MMK" = Just MMK
currency "MNT" = Just MNT
currency "MOP" = Just MOP
currency "MRO" = Just MRO
currency "MUR" = Just MUR
currency "MVR" = Just MVR
currency "MWK" = Just MWK
currency "MXN" = Just MXN
currency "MXV" = Just MXV
currency "MYR" = Just MYR
currency "MZN" = Just MZN
currency "NAD" = Just NAD
currency "NGN" = Just NGN
currency "NIO" = Just NIO
currency "NOK" = Just NOK
currency "NPR" = Just NPR
currency "NZD" = Just NZD
currency "OMR" = Just OMR
currency "PAB" = Just PAB
currency "PEN" = Just PEN
currency "PGK" = Just PGK
currency "PHP" = Just PHP
currency "PKR" = Just PKR
currency "PLN" = Just PLN
currency "PYG" = Just PYG
currency "QAR" = Just QAR
currency "RON" = Just RON
currency "RSD" = Just RSD
currency "RUB" = Just RUB
currency "RWF" = Just RWF
currency "SAR" = Just SAR
currency "SBD" = Just SBD
currency "SCR" = Just SCR
currency "SDG" = Just SDG
currency "SEK" = Just SEK
currency "SGD" = Just SGD
currency "SHP" = Just SHP
currency "SLL" = Just SLL
currency "SOS" = Just SOS
currency "SRD" = Just SRD
currency "SSP" = Just SSP
currency "STD" = Just STD
currency "SVC" = Just SVC
currency "SYP" = Just SYP
currency "SZL" = Just SZL
currency "THB" = Just THB
currency "TJS" = Just TJS
currency "TMT" = Just TMT
currency "TND" = Just TND
currency "TOP" = Just TOP
currency "TRY" = Just TRY
currency "TTD" = Just TTD
currency "TWD" = Just TWD
currency "TZS" = Just TZS
currency "UAH" = Just UAH
currency "UGX" = Just UGX
currency "USD" = Just USD
currency "USN" = Just USN
currency "UYI" = Just UYI
currency "UYU" = Just UYU
currency "UZS" = Just UZS
currency "VEF" = Just VEF
currency "VND" = Just VND
currency "VUV" = Just VUV
currency "WST" = Just WST
currency "XAF" = Just XAF
currency "XAG" = Just XAG
currency "XAU" = Just XAU
currency "XBA" = Just XBA
currency "XBB" = Just XBB
currency "XBC" = Just XBC
currency "XBD" = Just XBD
currency "XCD" = Just XCD
currency "XDR" = Just XDR
currency "XOF" = Just XOF
currency "XPD" = Just XPD
currency "XPF" = Just XPF
currency "XPT" = Just XPT
currency "XSU" = Just XSU
currency "XTS" = Just XTS
currency "XUA" = Just XUA
currency "XXX" = Just XXX
currency "YER" = Just YER
currency "ZAR" = Just ZAR
currency "ZMW" = Just ZMW
currency "ZWL" = Just ZWL
currency _     = Nothing

instance currencyParse :: Parse Currency where
  parse = do
     a <- upper
     b <- upper
     c <- upper
     maybe (P.fail "Unknown currency") pure $ currency (fromChars [a,b,c])

instance currencyRF :: ReadForeign Currency where
  readImpl f = do
     s <- readString f
     let c = currency s
     maybe (fail (ForeignError "Unknown currency")) pure c

