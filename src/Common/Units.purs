-- | Some units, a few from SI, a few custom.
-- TODO: Add hours and minutes
module Common.Units (DerivedUnit, parseUnit, convertUnit, divRemQ) where

import Prelude hiding (unit, Unit)

import Common.BigRational (BigRational(..), ceil, (%))
import Common.Utils (To)
import Common.Parse (class Parse)
import Control.Alt ((<|>))
import Control.Lazy (fix)
import Data.List (List(..), (:))
import Data.Maybe (Maybe(..), maybe)
import Data.Tuple (Tuple(..))
import JS.BigInt (fromTLInt)
import Parsing (fail)
import Parsing.Combinators (many, notFollowedBy, try)
import Parsing.String (char, string)
import Parsing.String.Basic (intDecimal, letter)
import Type.Proxy (Proxy(..))

data Dimension
  = Time
  | Length
  | Mass
  | Amount

derive instance dimensionEq :: Eq Dimension

data BaseUnit = BaseUnit Dimension

derive instance baseUnitEq :: Eq BaseUnit

-- | SI Prefixes up to 1960, enough for most cases
data Prefix
  = Pico -- 10^-12
  | Nano -- 10^-9
  | Micro -- 10^-6
  | Milli -- 10^-3
  | Centi -- 10^-2
  | Deci -- 10^-1
  | None -- 10^0
  | Deca -- 10^1
  | Hecto -- 10^2
  | Kilo -- 10^3
  | Mega -- 10^6
  | Giga -- 10^9
  | Tera -- 10^12

derive instance prefixEq :: Eq Prefix

data DerivedUnit
  = CommonUnit Prefix BaseUnit
  | Power Int DerivedUnit

derive instance derivedUnitEq :: Eq DerivedUnit

second :: BaseUnit
second = BaseUnit Time

meter :: BaseUnit
meter = BaseUnit Length

gram :: BaseUnit
gram = BaseUnit Mass

unit :: BaseUnit
unit = BaseUnit Amount

-- The Show instances for units follow the SI unit display convention, but mu is replaced with u.
instance Show Prefix where
  show Pico = "p"
  show Nano = "n"
  show Micro = "u"
  show Milli = "m"
  show Centi = "c"
  show Deci = "d"
  show None = ""
  show Deca = "da"
  show Hecto = "h"
  show Kilo = "k"
  show Mega = "M"
  show Giga = "G"
  show Tera = "T"

instance Show BaseUnit where
  show (BaseUnit Time) = "s"
  show (BaseUnit Length) = "m"
  show (BaseUnit Mass) = "g"
  show (BaseUnit Amount) = "un"

instance Show DerivedUnit where
  show (CommonUnit prefix base) =
    show prefix <> show base

  show (Power n u@(CommonUnit _ _)) =
    show u <> "^" <> show n

  show (Power n u) =
    "(" <> show u <> ")^" <> show n

baseUnit :: List Char -> Maybe BaseUnit
baseUnit ('s' : Nil) = Just second
baseUnit ('m' : Nil) = Just meter
baseUnit ('g' : Nil) = Just gram
baseUnit ('u' : 'n' : Nil) = Just unit
baseUnit _ = Nothing

commonUnit :: To DerivedUnit
commonUnit = do
  u <- many letter
  case u of
    'u' : 'n' : Nil  -> pure $ CommonUnit None unit
    's'       : Nil  -> pure $ CommonUnit None second
    'm'       : Nil  -> pure $ CommonUnit None meter
    'g'       : Nil  -> pure $ CommonUnit None gram
    'p'       : rest -> wrap Pico rest
    'n'       : rest -> wrap Nano rest
    'u'       : rest -> wrap Micro rest
    'm'       : rest -> wrap Milli rest
    'c'       : rest -> wrap Centi rest
    'd' : 'a' : rest -> wrap Deca rest
    'd'       : rest -> wrap Deci rest
    'h'       : rest -> wrap Hecto rest
    'k'       : rest -> wrap Kilo rest
    'M'       : rest -> wrap Mega rest
    'G'       : rest -> wrap Giga rest
    'T'       : rest -> wrap Tera rest
    _ -> fail "Invalid unit"
  where
  wrap u r = maybe (fail "Invalid unit") (CommonUnit u >>> pure) $ baseUnit r

power :: To DerivedUnit -> To DerivedUnit
power un = do
  u <- try commonUnit <|> (char '(' *> un <* char ')')
  n <- string "^" *> notFollowedBy (char '-') *> intDecimal
  pure $ Power n u

parseUnit :: To DerivedUnit
parseUnit = fix $ \un ->
  try (power un) <|> try (char '(' *> un <* char ')') <|> commonUnit

instance Parse DerivedUnit where
  parse = parseUnit

convertUnit :: Tuple BigRational DerivedUnit -> DerivedUnit -> Maybe BigRational
convertUnit (Tuple n u) u' | u == u' = Just n
convertUnit (Tuple n (Power pw un)) (Power pw' un') | pw == pw' = convertUnit (Tuple n un) un'
convertUnit (Tuple n (CommonUnit p base)) (CommonUnit p' base')
  | base == base' = Just $ n * (toFactor p / toFactor p')
convertUnit _ _ = Nothing

toFactor :: Prefix -> BigRational
toFactor Pico  = BigRational $ one % fromTLInt (Proxy :: Proxy 1_000_000_000_000)
toFactor Nano  = BigRational $ one % fromTLInt (Proxy :: Proxy 1_000_000_000)
toFactor Micro = BigRational $ one % fromTLInt (Proxy :: Proxy 1_000_000)
toFactor Milli = BigRational $ one % fromTLInt (Proxy :: Proxy 1_000)
toFactor Centi = BigRational $ one % fromTLInt (Proxy :: Proxy 100)
toFactor Deci  = BigRational $ one % fromTLInt (Proxy :: Proxy 10)
toFactor None  = BigRational $ one
toFactor Deca  = BigRational $ fromTLInt (Proxy :: Proxy 10) % one
toFactor Hecto = BigRational $ fromTLInt (Proxy :: Proxy 100) % one
toFactor Kilo  = BigRational $ fromTLInt (Proxy :: Proxy 1_000) % one
toFactor Mega  = BigRational $ fromTLInt (Proxy :: Proxy 1_000_000) % one
toFactor Giga  = BigRational $ fromTLInt (Proxy :: Proxy 1_000_000_000) % one
toFactor Tera  = BigRational $ fromTLInt (Proxy :: Proxy 1_000_000_000_000) % one

-- The Q stands for Quantity, a tuple of a number and a unit
divRemQ :: Tuple BigRational DerivedUnit
           -> Tuple BigRational DerivedUnit
           -> Maybe (Tuple BigRational BigRational)
divRemQ (Tuple n u) item = do
  n' <- convertUnit item u
  let di = ceil (n / n')
  pure $ Tuple di (di - n / n')

