module Common.BigRational (BigRational(..), fromInt, floor, ceil, module R) where

import Prelude

-- TODO: Don't just reexport Data.Ratio, change the important methods in order to make them compatible.
import Data.Ratio (Ratio, (%))
import Data.Ratio (Ratio, denominator, numerator, reduce, (%)) as R
import Data.Tuple (Tuple(..), fst)
import JS.BigInt (BigInt, fromInt) as B

-- Avoid orphan instances
newtype BigRational = BigRational (Ratio B.BigInt)

-- Inherit all the cool stuff
derive newtype instance eqBigRational :: Eq BigRational
derive newtype instance ordBigRational :: Ord BigRational
derive newtype instance semiringBigRational :: Semiring BigRational
derive newtype instance ringBigRational :: Ring BigRational
derive newtype instance commutativeRingBigRational :: CommutativeRing BigRational
derive newtype instance euclideanRingBigRational :: EuclideanRing BigRational

fromInt :: Int -> BigRational
fromInt n = BigRational $ B.fromInt n % one

floor :: BigRational -> BigRational
floor (BigRational b) = BigRational $ R.numerator b / R.denominator b % one

ceil :: BigRational -> BigRational
ceil (BigRational b) = 
  let n = R.numerator b
      d = R.denominator b in
    BigRational $ (n / d + if n `mod` d > zero then one else zero) % one

-- This algorithm was adapted from Fraction.js, converted to a more functional style.
showStep :: Tuple String BigRational -> Tuple String BigRational
showStep (Tuple s (BigRational b)) = 
  let n = R.numerator b
      d = R.denominator b in
    Tuple
     (s <> show (n / d))
     (BigRational $ ((n `mod` d) * B.fromInt 10) % d)

showStep' :: BigRational -> Tuple String BigRational
showStep' (BigRational b) =
   let n = R.numerator b                                                        
       d = R.denominator b in                                                   
     Tuple                                                                      
       (show (n / d) <> ".")                                                    
       (BigRational $ ((n `mod` d) * B.fromInt 10) % d)

instance Show BigRational where
  show n = showStep' n
         # showStep
         # showStep
         # fst

