module Common.Parse (class Parse, parse, class GParse, parse', genericParse) where

import Data.Generic.Rep

import Common.BigRational (BigRational(..), (%))
import Common.Utils (To, fromChars)
import Control.Alt ((<|>))
import Data.Int (pow)
import Data.List (List(..), length)
import Data.List.NonEmpty (toList)
import Data.Maybe (maybe)
import Data.Symbol (class IsSymbol, reflectSymbol)
import JS.BigInt (fromInt, fromString)
import Parsing (fail)
import Parsing.Combinators (many, many1, option, sepBy, try)
import Parsing.String (char, string)
import Parsing.String.Basic (digit, intDecimal, noneOf, number, oneOf, whiteSpace)
import Prelude (bind, discard, pure, void, ($), (*>), (<$>), (<*), (<>))
import Type.Proxy (Proxy(..))

class Parse a where
  parse :: To a

class GParse a where
  parse' :: To a

-- Common datatypes parsing
instance Parse Int where
  parse = intDecimal

instance Parse Number where
  parse = number

instance Parse BigRational where
  parse = do
     d <- many digit'
     d' <- option Nil $ try (char '.' *> (toList <$> many1 digit'))
     n <- maybe (fail "Invalid number") pure $ fromString $ fromChars (d <> d')
     pure $ BigRational $ n % (fromInt $ pow 10 (length d'))
     where
       digit' = many (char '_') *> digit <* many (char '_')

instance Parse String where
  parse = char '"' *> (fromChars <$> many (escaped <|> (noneOf ['"']))) <* char '"'
    where
      escaped = char '\\' *> oneOf ['"', '\\']

instance Parse a => Parse (List a) where
  parse = char '[' *> (parse `sepBy` char ',') <* char ']'

-- Generic Parsing
instance noConstructorsGP :: GParse NoConstructors where
  parse' = fail "No constructors"

instance noArgumentsGP :: GParse NoArguments where
  parse' = pure NoArguments

instance argumentGP :: Parse arg => GParse (Argument arg) where
  parse' = Argument <$> parse

instance sumGP :: (GParse a, GParse b) => GParse (Sum a b) where
  parse' = try (Inl <$> parse') <|> (Inr <$> parse')

instance productGP :: (GParse a, GParse b) => GParse (Product a b) where
  parse' = do
     a <- parse'
     void $ whiteSpace
     b <- parse'
     pure (Product a b)

instance constructorGP :: (IsSymbol name) => GParse (Constructor name NoArguments) where
  parse' = do
     void $ string (reflectSymbol (Proxy :: Proxy name))
     pure (Constructor NoArguments)

else instance constructorArgGP :: (IsSymbol name, GParse arg) => GParse (Constructor name arg) where
  parse' = do
     void $ string (reflectSymbol (Proxy :: Proxy name))
     void $ whiteSpace
     arg <- parse'
     pure (Constructor arg)

genericParse :: forall a rep. Generic a rep => GParse rep => To a
genericParse = to <$> parse'

