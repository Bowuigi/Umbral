module Common.Utils where

import Prelude

import Control.Monad.Except (Except, throwError)
import Data.Foldable (foldMap, class Foldable)
import Data.List (List(..), (:))
import Data.Maybe (Maybe(..))
import Data.String.CodeUnits (singleton)
import Parsing (Parser)

type To = Parser String

fromChars :: forall f. Foldable f => f Char -> String
fromChars = foldMap singleton

-- | Return the first element of a list satisfying the given predicate
lookupBy :: forall a. (a -> Boolean) -> List a -> Maybe a
lookupBy pred (x:xs) = if pred x then Just x else lookupBy pred xs
lookupBy _ Nil = Nothing

throwOnNothing :: forall a e. Maybe a -> e -> Except e a
throwOnNothing Nothing e = throwError e
throwOnNothing (Just a) _ = pure a

infixl 4 throwOnNothing as ^|!

