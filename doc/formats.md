# Formats

Description of commonly used formats in Umbral. The names, locations, prices, websites and any other piece of information used in examples are for educational purposes only.

## Info files

The equivalent of a project description. It is composed of the following:

```json
{
  "name": [1],
  "location": [1],
  "supervisor": [1],
  "company": [1],
  "currency": [2],
  "version": [3],
  "sources": [4]
}
```

1. A (usually short) string representing the given field, generally for user display.
2. ISO-4217 currency code represented as a JSON string, used for convertions
3. A single number, starting from 0.
4. A JSON array of the form `{"url": URL, "name": NAME, "paymentMethod": PAYMENT_METHOD}`, where PAYMENT_METHOD is one of "credit"/"debit"/"cash" or an arbitrary string, and the rest are strings

Those fields are not case sensitive.

Here is an example:

```json
{
  "name": "Very important building",
  "location": "Null Island",
  "supervisor": "John Smith",
  "currency": "USD",
  "version": 1,
  "sources": [
    {
      "url": "iron.com/list.tsv",
      "name": "Iron",
      "paymentMethod": "debit"
    },
    {
      "url": "concrete.co/material.tsv",
      "name": "Concrete",
      "paymentMethod": "credit"
    },
    {
      "url": "bank.org/lists/construction.tsv",
      "name": "Reference",
      "paymentMethod": "cash"
    }
  ]
}
```

## Price lists

Used to represent price lists, they are TSV tables with the following headers (case insensitive, not optional):

Unless specified, any of those may contain any UTF-8 character except a tab.

- ID: Numbers, used as an identifier (they must not change), no specific order required.
- Name: Name of the product, in singular form. For example: 10mm Gray Iron Sheet.
- UPP: How much units are in each package. Mnemonic: Units Per Package.
- Unit: Any SI unit or "un" for an object, for example, 3 un of nail are 3 nails. Note, SI prefixes for "un" are supported.
- PPP: Price Per Package.
- Currency: ISO-4127 currency code for the currency used in the PPMU.

Example: (replace commas with tabs to get a valid file)

```csv
ID,Name,UPP,Unit,PPP,Currency
0,3cm Nail,1,kg,5,USD
1,2cm Nail,100,un,13,USD
2,10x10x1.2mm Pipe,6,m,12,USD
3,Metal wire N16,1,kg,7,USD
```

## Tax list

All the taxes applied, in detail. Generally this is just copied around between budgets.

TSV file (here shown as a CSV for clarity), the headers are case insensitive but not optional.

For example, some common taxes in Argentina:

```csv
Name,Type,Amount,Condition,Overrides
VAT,Percentage,21,None,None
Foreign Currency Tax,Percentage,30,NotCurrency [ARS],None
Dollar Credit Card Tax,Percentage,35,All [Currency [USD], Method Credit],None
```

A bit more random examples to show the expressivity of the format:

```csv
Name,Type,Amount,Condition,Overrides
Inspection,Fixed USD,300,None,None
Special VAT,Percentage,10,Method Cash,VAT
Just In Case,Percentage,15,None,None
```

I would recommend to add a "Just in case" tax because costs can change long-term.

- Name: Identifies the tax, used in Overrides
- Currency: Either 'Percentage' for a percentage or "Fixed CURRENCY" for a constant tax.
- Amount: How much Currency does the tax cost.
- Overrides: Name that it overrides if it is active, None for none.
- Condition: A condition to apply the tax (Case sensitive), it is of the form:
  None: No condition.
  Method Credit/Debit/Cash/(Other "METHOD"): If such payment method is used, apply it.
  Currency [...]: If such currencies are used, apply it.
  NotCurrency [...]: If such currencies are not used, apply it.
  Exceeds n c: If price exceeds n (in currency c), apply it.
  Below n c: If price is below n (in currency c), apply it.
  All [Condition, ...]: If all conditions succeed, apply it.
  Any [Condition, ...]: If at least one condition succeeds, apply it

## Costs list

TSV files representing costs, the headers are case insensitive but not optional.

```csv
Source,ID,Category,Amount,Unit
```

- Source: The source name as specified in the Info file.
- ID: The ID of an item on the price list marked as the source.
- Category: Category of current cost. Used for ordering and display.
- Amount: Amount of units of the item to buy.
- Unit: SI unit or "un".

## Budget

The budget file packs all the previous files together, it is composed of multiple headers, containing information about each contained file.

The header either starts with "##" (info, taxes, costs) or with "#!" (price lists), followed by the type or the list name respectively.

It is much easier to understand this with an example:

As usual, replace commas in TSV files to make them actually valid.

```json
## Info
{
  "name": "Very important building",
  "location": "Null Island",
  "supervisor": "John Smith",
  "currency": "USD",
  "version": 1,
  "sources": [
    {
      "url": "metal.com/list.tsv",
      "name": "Metal Inc.",
      "paymentMethod": "debit"
    },
    {
      "url": "pipeland.org/pipes.tsv",
      "name": "Metal Inc.",
      "paymentMethod": "debit"
    }
  ]
}
## Taxes
Name,Type,Amount,Condition,Overrides
VAT,Percentage,21,None,None
Foreign Currency Tax,Percentage,30,NotCurrency [ARS],None
## Costs
Source,ID,Category,Amount,Unit
Metal Inc.,1,Kitchen repairs,30,un
Pipeland,0,Bathroom repairs,6,m
#! Metal Inc.
ID,Name,UPP,Unit,PPP,Currency
0,3cm Nail,1,kg,5,USD
1,2cm Nail,100,un,13,USD
3,Metal wire N16,1,kg,7,USD
#! Pipeland
ID,Name,UPP,Unit,PPP,Currency
0,10x10x1.2mm Pipe,6,m,12,USD
1,10x10x3mm Pipe,3,m,12,USD
```

