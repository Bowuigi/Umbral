# General description of Umbral

It is a distributed VCS, it works on "Budgets", which are made of the following files:

A info file
A costs list
A tax list
One or more price lists

The info file contains source remotes for the price lists, which should be updated on demand

The "Budget" should be distributed as an archive.

The possible operations are:

Price lists:
- update: Update price lists from the source remotes in the building file

Version control:
- merge: Merge two budgets into one, accounting for versions

Presentation:
- render-pdf: Render budget to PDF file

LibreOffice compatibility:
- render-csv: The whole budget is rendered into a TSV (which is read only).

