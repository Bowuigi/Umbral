module Test.Main where

import Prelude

import Common.Utils (To)
import Control.Monad.Except (runExcept)
import Data.Either (Either(..), either)
import Data.Foldable (intercalate)
import Data.List (List)
import Effect (Effect)
import Effect.Console (log)
import Format.Budget (budget)
import Format.Export (export, exportTSV, total)
import Node.Encoding (Encoding(..))
import Node.FS.Sync (readTextFile, writeTextFile)
import Parsing (runParser)
import Parsing.String (parseErrorHuman)
import Types.Formats (Exported)

parseE :: forall a. String -> To a -> Either String a
parseE s p = case runParser s p of
  Left err -> Left $ intercalate "\n" $ parseErrorHuman s 20 err
  Right v -> Right v

infixr 5 parseE as #~

exportIt :: List Exported -> Effect Unit
exportIt e = do
  writeTextFile UTF8 "output.tsv" $ exportTSV e
  writeTextFile UTF8 "total.txt" $ show $ total e

main :: Effect Unit
main = do
  f <- readTextFile UTF8 "example.um"
  case f #~ budget of
    Left err -> log err
    Right b -> either log exportIt $ runExcept (export b)

